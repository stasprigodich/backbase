//
//  CityListViewController.swift
//  backbase
//
//  Created by Stanislav Prigodich on 07/09/2018.
//  Copyright © 2018 prigodich. All rights reserved.
//

import UIKit

class CityListViewController: UIViewController {
    
    @IBOutlet weak var searchTextField: SearchTextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var emptySearchLabel: UILabel!
    
    fileprivate var cities = [City]()
    
    private var isLoading = true {
        didSet {
            if !isLoading {
                main {
                    self.activityIndicator.stopAnimating()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCities()
        searchTextField.addTarget(self, action: #selector(searchTextFieldChanged), for: .editingChanged)
    }
    
    @objc func searchTextFieldChanged(_ textField: SearchTextField) {
        let text = textField.text ?? ""
        searchCities(with: text)
    }
    
    private func searchCities(with text: String) {
        guard !isLoading else {
            return
        }
        background {
            let searchedCities = CitiesManager.shared.search(with: text)
            main {
                if text == self.searchTextField.text ?? "" {
                    self.updateUI(with: searchedCities)
                }
            }
        }
    }
    
    private func loadCities() {
        background {
            let allCities = CitiesManager.shared.search(with: "")
            main {
                self.isLoading = false
                if let searchText = self.searchTextField.text, !searchText.isEmpty {
                    self.searchCities(with: searchText)
                } else {
                    self.updateUI(with: allCities)
                }
            }
        }
    }
    
    private func updateUI(with cities: [City]) {
        tableView.setContentOffset(CGPoint.zero, animated: false)
        self.cities = cities
        tableView.reloadData()
        emptySearchLabel.isHidden = self.cities.count > 0
    }
    
    private func hideKeyboard() {
        searchTextField.resignFirstResponder()
    }
    
}

extension CityListViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CityTableViewCell.identifier) as! CityTableViewCell
        cell.configure(with: cities[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hideKeyboard()
        navigationController?.pushViewController(CityMapViewController.instance(with: cities[indexPath.row]), animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        hideKeyboard()
    }
    
}

extension CityListViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyboard()
        return true
    }
    
}
