//
//  CityMapViewController.swift
//  backbase
//
//  Created by Stanislav Prigodich on 07/09/2018.
//  Copyright © 2018 prigodich. All rights reserved.
//

import UIKit
import MapKit

class CityMapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var backButton: UIButton!
    
    private var city: City!
    
    static func instance(with city: City) -> CityMapViewController {
        let cityMapViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CityMapViewController") as! CityMapViewController
        cityMapViewController.city = city
        return cityMapViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initMap()
    }
    
    private func initMap() {
        let location = CLLocationCoordinate2D(latitude: city.location.lat, longitude: city.location.lon)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
