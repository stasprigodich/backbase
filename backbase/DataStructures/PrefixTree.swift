//
//  PrefixTree.swift
//  backbase
//
//  Created by Stanislav Prigodich on 08/09/2018.
//  Copyright © 2018 prigodich. All rights reserved.
//


import Foundation

protocol PrefixTreeEquatable {
    var displayTitle: String { get }
}

//I preprocess the list of cities into Prefix Tree, because the complexity of searching strings is O(m), where m is the average length of a string, so that allows us to display the filtered list without delay

class PrefixTree<T:PrefixTreeEquatable> {
    
    typealias Node = PrefixTreeNode<T>
    fileprivate let root: Node
    private var sortedItems = [T]()
    
    init(items: [T]) {
        root = Node()
        sortedItems = items.sorted{$0.displayTitle.caseInsensitiveCompare($1.displayTitle) == .orderedAscending}.filter({!$0.displayTitle.isEmpty})
        sortedItems.forEach({self.insert(item: $0)})
    }
    
    func search(with text: String) -> [T] {
        guard !text.isEmpty else {
            return sortedItems
        }
        var items = [T]()
        let prefix = text.lowercased()
        if let lastNode = lastNode(with: prefix) {
            if lastNode.hasItem {
                items.append(contentsOf: lastNode.items)
            }
            for character in lastNode.childrenOrderedCharacters {
                let childNode = lastNode.children[character]!
                let childItems = subTree(for: childNode, prefix: prefix)
                items += childItems
            }
        }
        return items
    }
    
    private func insert(item: T) {
        guard !item.displayTitle.isEmpty else {
            return
        }
        var currentNode = root
        for character in item.displayTitle.lowercased() {
            if let childNode = currentNode.children[character] {
                currentNode = childNode
            } else {
                currentNode.addChild(with: character)
                currentNode = currentNode.children[character]!
            }
        }
        currentNode.insert(item: item)
        currentNode.hasItem = true
    }
    
    private func lastNode(with text: String) -> Node? {
        var currentNode = root
        for character in text.lowercased() {
            guard let childNode = currentNode.children[character] else {
                return nil
            }
            currentNode = childNode
        }
        return currentNode
    }
    
    private func subTree(for node: Node, prefix: String) -> [T] {
        var items = [T]()
        var currentPrefix = prefix
        if let character = node.character {
            currentPrefix.append(character)
        }
        if node.hasItem {
            items.append(contentsOf: node.items)
        }
        for character in node.childrenOrderedCharacters {
            let childNode = node.children[character]!
            let childItems = subTree(for: childNode, prefix: currentPrefix)
            items += childItems
        }
        return items
    }
    
}

class PrefixTreeNode<T: PrefixTreeEquatable> {
    
    fileprivate var character: Character?
    fileprivate var items = [T]()
    fileprivate weak var parentNode: PrefixTreeNode?
    fileprivate var children: [Character: PrefixTreeNode] = [:]
    fileprivate var childrenOrderedCharacters = [Character]()
    fileprivate var hasItem = false

    init(character: Character? = nil, parentNode: PrefixTreeNode? = nil) {
        self.character = character
        self.parentNode = parentNode
    }
    
    fileprivate func addChild(with character: Character) {
        guard children[character] == nil else {
            return
        }
        children[character] = PrefixTreeNode(character: character, parentNode: self)
        childrenOrderedCharacters.append(character)
    }
    
    fileprivate func insert(item: T) {
        items.append(item)
    }
    
}
