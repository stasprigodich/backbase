//
//  Utils.swift
//  backbase
//
//  Created by Stanislav Prigodich on 07/09/2018.
//  Copyright © 2018 prigodich. All rights reserved.
//

import Foundation

func background(work: @escaping () -> ()) {
    DispatchQueue.global(qos: .userInitiated).async {
        work()
    }
}

func main(work: @escaping () -> ()) {
    DispatchQueue.main.async {
        work()
    }
}
