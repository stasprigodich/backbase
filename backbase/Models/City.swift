//
//  City.swift
//  backbase
//
//  Created by Stanislav Prigodich on 07/09/2018.
//  Copyright © 2018 prigodich. All rights reserved.
//

import Foundation

struct City: Decodable, PrefixTreeEquatable {
            
    let country: String
    let name: String
    let id: Int
    let location: Location
    
    enum CodingKeys: String, CodingKey {
        case country
        case name
        case id = "_id"
        case location = "coord"
    }
    
    var displayTitle: String {
        return "\(name), \(country)"
    }
    
}
