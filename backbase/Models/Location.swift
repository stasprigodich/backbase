//
//  Location.swift
//  backbase
//
//  Created by Stanislav Prigodich on 07/09/2018.
//  Copyright © 2018 prigodich. All rights reserved.
//

import Foundation

struct Location: Decodable {
    let lon: Double
    let lat: Double
}
