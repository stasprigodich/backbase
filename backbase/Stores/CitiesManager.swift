//
//  CitiesManager.swift
//  backbase
//
//  Created by Stanislav Prigodich on 07/09/2018.
//  Copyright © 2018 prigodich. All rights reserved.
//

import Foundation

class CitiesManager {
    
    static let shared = CitiesManager()
    
    private var prefixTree: PrefixTree<City>!

    init() {
        prefixTree = PrefixTree<City>(items: allCities)
    }
    
    private lazy var allCities: [City] = {
        if let path = Bundle.main.path(forResource: "cities", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                guard let cities = try? JSONDecoder().decode(Array<City>.self, from: data) else {
                    return [City]()
                }
                return cities
            } catch { }
        }
        return [City]()
    }()
    
    func search(with text: String) -> [City] {
        return prefixTree.search(with: text)
    }
    
}
