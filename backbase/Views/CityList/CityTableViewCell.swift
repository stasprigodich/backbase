//
//  CityTableViewCell.swift
//  backbase
//
//  Created by Stanislav Prigodich on 07/09/2018.
//  Copyright © 2018 prigodich. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {
    
    static let identifier = "CityTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func configure(with city: City) {
        titleLabel.text = city.displayTitle
    }
    
}
