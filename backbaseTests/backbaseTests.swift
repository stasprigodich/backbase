//
//  backbaseTests.swift
//  backbaseTests
//
//  Created by Stanislav Prigodich on 07/09/2018.
//  Copyright © 2018 prigodich. All rights reserved.
//

import XCTest
@testable import backbase

struct PrefixTreeItem: PrefixTreeEquatable {
    var displayTitle: String
}

class backbaseTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testPrefixTreeCreate() {
        let prefixTree = PrefixTree(items: [PrefixTreeItem]())
        let searchResult = prefixTree.search(with: "")
        XCTAssertEqual(searchResult.count, 0)
    }
    
    func testEmptyPrefixSearch() {
        let items = [PrefixTreeItem(displayTitle: "Amsterdam"), PrefixTreeItem(displayTitle: "Paris")]
        let prefixTree = PrefixTree(items: items)
        let searchResult = prefixTree.search(with: "")
        XCTAssertEqual(searchResult.count, items.count)
    }
    
    func testPrefixTreeSearch() {
        let items = [
            PrefixTreeItem(displayTitle: "Paris"),
            PrefixTreeItem(displayTitle: "OSLO"),
            PrefixTreeItem(displayTitle: "moscow"),
            PrefixTreeItem(displayTitle: "Amsterdam"),
            PrefixTreeItem(displayTitle: "Mexico")
        ]
        let prefixTree = PrefixTree(items: items)
        let searchResult = prefixTree.search(with: "m")
        XCTAssertEqual(searchResult.count, 2)
        XCTAssertEqual(searchResult[0].displayTitle, "Mexico")
        XCTAssertEqual(searchResult[1].displayTitle, "moscow")
    }
    
    func testSamePrefixTreeItems() {
        let items = [
            PrefixTreeItem(displayTitle: "Paris"),
            PrefixTreeItem(displayTitle: "Paris"),
            PrefixTreeItem(displayTitle: "moscow"),
            PrefixTreeItem(displayTitle: "Amsterdam"),
            PrefixTreeItem(displayTitle: "PARIS")
        ]
        let prefixTree = PrefixTree(items: items)
        let searchResult = prefixTree.search(with: "paris")
        XCTAssertEqual(searchResult.count, 3)
        for resultItem in searchResult {
            XCTAssertEqual(resultItem.displayTitle.lowercased(), "Paris".lowercased())
        }
    }
    
    func testPrefixTreeAlphabeticalOrder() {
        let items = [
            PrefixTreeItem(displayTitle: "New York"),
            PrefixTreeItem(displayTitle: "Norway"),
            PrefixTreeItem(displayTitle: "Netherlands"),
            PrefixTreeItem(displayTitle: "New Orleans"),
            PrefixTreeItem(displayTitle: "Newcastle")
        ]
        let prefixTree = PrefixTree(items: items)
        let searchResult = prefixTree.search(with: "n")
        XCTAssertEqual(searchResult.count, 5)
        XCTAssertEqual(searchResult[0].displayTitle, "Netherlands")
        XCTAssertEqual(searchResult[1].displayTitle, "New Orleans")
        XCTAssertEqual(searchResult[2].displayTitle, "New York")
        XCTAssertEqual(searchResult[3].displayTitle, "Newcastle")
        XCTAssertEqual(searchResult[4].displayTitle, "Norway")
    }
    
    func testPrefixTreeNoFound() {
        let items = [
            PrefixTreeItem(displayTitle: "Paris"),
            PrefixTreeItem(displayTitle: "Paris"),
            PrefixTreeItem(displayTitle: "moscow"),
            PrefixTreeItem(displayTitle: "Amsterdam"),
            PrefixTreeItem(displayTitle: "PARIS")
        ]
        let prefixTree = PrefixTree(items: items)
        let searchResult = prefixTree.search(with: "Mi")
        XCTAssertEqual(searchResult.count, 0)
    }
    
    
    func testEmptyItemPrefixTree() {
        let prefixTree = PrefixTree(items: [PrefixTreeItem(displayTitle: ""), PrefixTreeItem(displayTitle: "")])
        let searchResult = prefixTree.search(with: "")
        XCTAssertEqual(searchResult.count, 0)
    }
    
}
